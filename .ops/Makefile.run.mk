docker_run:
	docker run -d --name=mosquitto_test_run -p 1883:1883 homesmarthome/mosquitto:latest
	docker run -d \
	  --name=amazonecho_test_run \
		-p 52001:52001 \
    	-p 52002:52002 \
    	-p 52003:52003 \
	  	-v $(PWD)/.env:/env \
		-v $(PWD)/test/amazon.json:/devices.json \
		--link mosquitto_test_run:mosquitto \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep amazonecho_test_run

docker_stop:
	docker rm -f amazonecho_test_run mosquitto_test_run 2> /dev/null; true
	docker rm -f mosquitto_test_run 2> /dev/null; true