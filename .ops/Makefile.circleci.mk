circleci-config:
	mkdir -p .build
	circleci config validate
	circleci config pack .circleci/config.yml > .build/config.pack.yml
	circleci config process .build/config.pack.yml > .build/config.yml

circleci-lint: circleci-config
	circleci build \
		--job docker/hadolint \
		-c .build/config.yml \
		--skip-checkout

circleci-shellcheck: circleci-config
	circleci build \
		--job shellcheck/check \
		-c .build/config.yml \
		--skip-checkout

circleci-vulnerability-checker: circleci-config
	circleci build \
		--job vulnerability-checker/scan \
		-c .build/config.yml \
		--skip-checkout
	
circleci-build: circleci-config
	circleci build \
		--job fvsqr/build \
		-c .build/config.yml \
		--skip-checkout \
		--env CIRCLE_PROJECT_REPONAME=$$CIRCLE_PROJECT_REPONAME \
		--env CIRCLE_PROJECT_USERNAME=$$CIRCLE_PROJECT_USERNAME \
		--env DOCKER_USER=$$DOCKER_USER \
		--env DOCKER_PASS=$$DOCKER_PASS \
		--env DOCKER_IMAGE_NAMESPACE=$$DOCKER_IMAGE_NAMESPACE

circleci-test-goss: circleci-config
	circleci build \
		--job test-goss \
		-c .build/config.yml \
		--skip-checkout \
		--env CIRCLE_PROJECT_REPONAME=$$CIRCLE_PROJECT_REPONAME \
		--env CIRCLE_PROJECT_USERNAME=$$CIRCLE_PROJECT_USERNAME \
		--env DOCKER_USER=$$DOCKER_USER \
		--env DOCKER_PASS=$$DOCKER_PASS