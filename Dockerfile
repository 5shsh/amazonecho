#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=python
ARG BASE_IMAGE_VERSION=2.7.16-alpine3.9

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

RUN apk add --no-cache wget unzip
RUN wget --no-check-certificate "https://github.com/toddmedema/echo/archive/master.zip" -O echo.zip
RUN unzip echo.zip -d echo

COPY image_files /
RUN pip install -r requirements.txt

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
COPY image_files /

COPY --from=builder /echo/echo-master/fauxmo.py /echo2mqtt/
COPY --from=builder /echo/echo-master/debounce_handler.py /echo2mqtt/
COPY --from=builder /root/.cache /root/.cache

RUN pip install -r requirements.txt && rm -rf /root/.cache

EXPOSE 52001 52002 52003 52004 52005 52006 52007 52008 52009 52010

ENTRYPOINT [ "/entrypoint.sh" ]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL   org.label-schema.name="homesmarthome/amazonecho" \
        org.label-schema.description="Wink clone for enabling Amazon echo to send on/off commands via MQTT" \
        org.label-schema.url="homesmarthome.5square.de" \
        org.label-schema.vcs-ref="$VCS_REF" \
        org.label-schema.vcs-url="$VCS_URL" \
        org.label-schema.vendor="fvsqr" \
        org.label-schema.version=$VERSION \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.schema-version="1.0" \
        org.label-schema.docker.cmd="TBD"