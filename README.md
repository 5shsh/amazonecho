# amazonecho

[![](https://images.microbadger.com/badges/image/homesmarthome/amazonecho.svg)](https://hub.docker.com/r/homesmarthome/amazonecho/ "Amazonecho on Docker Hub")

[![](https://images.microbadger.com/badges/version/homesmarthome/amazonecho.svg)](https://hub.docker.com/r/homesmarthome/amazonecho/ "Amazonecho on Docker Hub")

Emulated Wink functionality that allows Amazon Echo to switch devices without controlling from the cloud. For every device a port has to be mapped, and an entry to ```devices.json``` has to be added. ```devices.json```has to be mounted from the host system.
Environment file has to be specified: ```MQTT_HOST``` and ```MQTT_PORT``` have to be set.

## Shuttle Service to start
As a Docker Swarm service cannot be started in network mode `host` amazonecho_shuttle has to be used in order to start a amazonecho container.

## Run
```
docker run -d \
    --restart=unless-stopped \
    --name=amazonecho \
    --env-file /config/env \
    -v /config/amazonecho/devices.json:/devices.json \
    -p 52001:52001 \
    -p 52002:52002 \
    -p 52003:52003 \
    homesmarthome/amazonecho:1.1.1
```

### devices.json
```
{
    "fz_light": {
        "port": 52001,
        "alexa_name": "Licht",
        "topic": "/amazonecho/actor/fz_light"
    },
    "fz_tv": {
        "port": 52002,
        "alexa_name": "Fernseher",
        "topic": "/amazonecho/actor/fz_tv"
    },
    "fz_desktop": {
        "port": 52003,
        "alexa_name": "Schreibtisch",
        "topic": "/amazonecho/actor/fz_desktop"
    }
}
```