#!/usr/bin/env python

# Test

import subprocess
import paho.mqtt.client as mqtt
import fauxmo
from debounce_handler import debounce_handler
import threading
import logging
import time
import json
import os

logging.basicConfig(level=logging.DEBUG)

# ---------- Network constants -----------
MQTT_HOST = os.environ['MQTT_HOST']
MQTT_PORT = os.environ['MQTT_PORT']

with open('/devices.json') as json_data:
    DEVICES = json.load(json_data)

def dbg(msg):
    logging.debug(msg)

class device_handler(debounce_handler):

    def __init__(self, mqtt, name, topic):
        debounce_handler.__init__(self)
        self.mqtt = mqtt
        self.name = name
        self.topic = topic

    def act(self, client_address, payload, what):
        if payload == True:
            payload = "ON"
        elif payload == False:
            payload = "OFF"
        dbg("name: %s, payload: %s, topic: %s, what: %s" % (self.name, payload, self.topic, what))
        self.mqtt.publish(self.topic, payload)
        return True

if __name__ == "__main__":
    # Startup the MQTT client in a separate thread
    client = mqtt.Client()
    client.connect(MQTT_HOST, MQTT_PORT, 60)
    ct = threading.Thread(target=client.loop_forever)
    ct.daemon = True
    ct.start()

    # Startup the fauxmo server
    fauxmo.DEBUG = True
    p = fauxmo.poller()
    u = fauxmo.upnp_broadcast_responder()
    u.init_socket()
    p.add(u)

    for device_id, device in DEVICES.items():
        h = device_handler(client, device['alexa_name'], device['topic'])
        fauxmo.fauxmo(device['alexa_name'], u, p, None, device['port'], h)

    # Loop and poll for incoming Echo requests
    #logging.debug("Entering fauxmo polling loop")
    while True:
        try:
            # Allow time for a ctrl-c to stop the process
            p.poll(100)
            time.sleep(0.1)
        except Exception, e:
            logging.critical("Critical exception: " + str(e))
            break
